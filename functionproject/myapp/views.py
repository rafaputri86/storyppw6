from django.shortcuts import render, redirect
#from django.http import HttpResponse
from .models import Masukan
from . import forms

def status(request):
    if request.method == 'POST':
        form = forms.CommentForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('index')
    else:
        form = forms.CommentForm()
        return render(request, 'index.html', {'form': form, 'list_status' : Masukan.objects.all()})


    